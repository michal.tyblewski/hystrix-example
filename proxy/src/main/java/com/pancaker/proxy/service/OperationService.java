package com.pancaker.proxy.service;

import com.pancaker.common.dto.OperationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OperationService {


    private final OperationRestService restService;

    public OperationService(OperationRestService restService) {
        this.restService = restService;
    }

    public OperationResponse withoutAnyErrorHandling() {
        return restService.add(1f, 2f);
    }

    public OperationResponse withoutAnyErrorRandomException() {
        return restService.addWithRandomFailure(1f, 2f);
    }
}
