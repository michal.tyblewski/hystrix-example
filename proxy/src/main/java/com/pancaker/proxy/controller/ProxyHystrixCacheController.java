package com.pancaker.proxy.controller;

import com.pancaker.common.dto.OperationRequest;
import com.pancaker.common.dto.OperationResponse;
import com.pancaker.proxy.service.OperationHystrixCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/proxy")
public class ProxyHystrixCacheController {

    //region initx
    private final OperationHystrixCacheService operationHystrixCacheService;

    public ProxyHystrixCacheController(OperationHystrixCacheService operationHystrixCacheService) {
        this.operationHystrixCacheService = operationHystrixCacheService;
    }
    //endregion

    //region hystrix + cache

    @PostMapping("/with-many-keys")
    public OperationResponse withManyKeys(@RequestBody OperationRequest request) {
        return operationHystrixCacheService.withManyKeys(request.getLeftParameter(), request.getRightParameter());
    }

    @PostMapping("/with-many-keys-one-cache-key")
    public OperationResponse withManyKeysOneCacheKey(@RequestBody OperationRequest request) {
        return operationHystrixCacheService.withManyKeysOneCacheKey(request.getLeftParameter(), request.getRightParameter());
    }

    @PostMapping("/with-many-keys-from-object")
    public OperationResponse withManyKeysFromObject(@RequestBody OperationRequest request) {
        return operationHystrixCacheService.withManyKeysFromObject(request);
    }

    @PostMapping("/with-many-keys-one-cache-key-from-object")
    public OperationResponse withManyKeysOneCacheKeyFromObject(@RequestBody OperationRequest request) {
        return operationHystrixCacheService.withManyKeysOneCacheKeyFromObject(request);
    }

    //endregion

}
